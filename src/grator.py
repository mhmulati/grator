#!/usr/bin/env python
# encoding: utf-8

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# Author: Mauro Henrique Mulati
# Date: 07-AUG-2015, 18-DEC-2024

# Uso em linha de comando do GNU/Linux:
# $ chmod +x grator
# $ ./grator param_1 param2 ... paramk > graph.in 2> graph.log
# ou
# $ python grator param_1 param2 ... paramk > graph.in 2> graph.log

import random
import sys
# import sets
from ast import literal_eval


def gerar_grafo(n = 5, perc_arest = 0.5, orientado = True, conexo_ou_semi = True, pesos = False, peso_inf = 0, peso_sup = 0, peso_int = True, perc_arest_arest_anti_paralel = 0.01, perc_vert_arests_laco = 0.005, aciclico_se_orientado_e_sem_lacos_e_sem_anti_paralel = False):
	# Formato: tgf modificado.
	# Definição de semiconexo consta no exercício 22.5-7, na pág. 452 de Cormen, 3a ed.
	# NOTE 1: quando conexo_ou_semi = True, a construção do grafo sempre ocorre de forma conexa (após cada iteração do laço for interno). Desse modo, teremos por exemplo, que sempre haverá uma aresta entre o primeiro e o segundo vértices.
	# NOTE 2: quando conexo_ou_semi = False, ainda pode ocorrer de o grafo ser conexo ou semiconexo. A variável apenas indica que a função não vai criar arestas extraordinárias para forçar que o grafo seja conexo ou semi.

	verts            = []
	vizinho_de_menor = []  # u é vizinho de t, tal que t < u
	m = 0                  # Qt. arestas entre vértices, sem contar antiparalelas
	p = 0                  # Qt. arestas anti-paralelas. Em grafos não-orientados, temos que p = 0. Em grafos orientados, temos 0 <= p <= (n*(n-1))/2.
	l = 0                  # Qt. arestas laços. Em grafos não-orientados, temos l = 0. Em grafos orientados, temos 0 <= l <= n.
	c = 0                  # Qt. arestas extra-ordinárias para forçar {conexão|semi-conexão} do grafo.

	# Log de dados para geração do grafo
	print("n:", n, " % arests:", perc_arest * 100, " Orientado:", orientado, " {Conexo|Semiconexo}:", conexo_ou_semi, " Pesos:", pesos, " Peso infer:", peso_inf, " Peso super:", peso_sup, " Peso int:", peso_int, " % arests c/arest anti-paral:", perc_arest_arest_anti_paralel * 100, " % verts c/ arest laço:", perc_vert_arests_laco * 100, file=sys.stderr)

	print("Qt arests {grafo|versão não-orientado} compl:", (n * (n - 1))/2, file=sys.stderr) # http://pt.wikipedia.org/wiki/Grafo_completo
	print("Qt min arests p/grafo ser {conexo|semiconexo}:", n - 1, file=sys.stderr)          # Teorema B.2, Cormen 3.a ed., pág. 851. Se o grafo tiver a quantidade mínima de arestas, isto garante que ele é acíclico.

	# if not orientado:      # Imprimir orientação do grafo
	#	print "1"
	# else:
	#	print "2"
	# print "#"

	## print n # Imprimir nr de vértices
	## print "M" # Imprimir "placeholder" p/ nr de arestas
	## print "#"

	for u in range(0, n):  # Gerar vértices
		verts.append([])
		vizinho_de_menor.append(False)
		## print u + 1 # Nos vértices, trabalhando internamente no intervalo 0..n - 1, mas imprimindo no intervalo 1..n
	## print "#"

	log_grafo(n, verts);   # Log da estrutura de listas de adjacência no ponto atual, i.e., antes da inclusão de arestas

	for u in range(0, n):  # Gerar arestas
		for v in range(u + 1, n):
			x = u
			y = v
			if not (orientado and perc_vert_arests_laco == 0.0 and perc_arest_arest_anti_paralel == 0.0 and aciclico_se_orientado_e_sem_lacos_e_sem_anti_paralel):
				# Exchange
				rand = random.uniform(0, 1)
				if rand < 0.5:
					aux = x
					x   = y
					y   = aux
			print("e = ", aresta_as_string_only_for_log(x, y), " ?", file=sys.stderr)
			rand = random.uniform(0, 1)
			if rand < perc_arest:                  # Gerar aresta (x, y)
				print("YES - rand: ", rand, file=sys.stderr)
				peso = gerar_peso(peso_inf, peso_sup, peso_int, pesos)
				incluir_aresta(verts, x, y, peso, pesos)
				m = m + 1
				vizinho_de_menor[v] = True

				if orientado and perc_arest_arest_anti_paralel > 0:  # Gerar a aresta antiparalela (y, x), que só existe em grafo orientado. Sua correlata já deve ter sido gerada.
					rand = random.uniform(0, 1)
					if rand < perc_arest_arest_anti_paralel:
						print("p = ", aresta_as_string_only_for_log(y, x), " - rand: ", rand, file=sys.stderr)
						peso = gerar_peso(peso_inf, peso_sup, peso_int, pesos)
						incluir_aresta(verts, y, x, peso, pesos)
						p = p + 1

		if orientado and perc_vert_arests_laco > 0:  # Arestas laço no vértice u, que só existem em grafo orientados
			rand = random.uniform(0, 1)
			if rand < perc_vert_arests_laco:
				print("l = ", aresta_as_string_only_for_log(u, v), " - rand: ", rand, file=sys.stderr)
				peso = gerar_peso(peso_inf, peso_sup, peso_int, pesos)
				incluir_aresta(verts, u, u, peso, pesos)
				l = l + 1

		if conexo_ou_semi and ( not vizinho_de_menor[u] ) and ( u > 0 ):
			t_cands = range(0, u)  # [0, u)
			print("t_cands:", t_cands, file=sys.stderr)
			t = random.choice(t_cands)
			# Inserir uma aresta de vértice menor, t, selecionado aleatoriamente para u
			x = t
			y = u
			if not (orientado and perc_vert_arests_laco == 0.0 and perc_arest_arest_anti_paralel == 0.0 and aciclico_se_orientado_e_sem_lacos_e_sem_anti_paralel):
				# Exchange
				if rand < 0.5:
					aux = x
					x   = y
					y   = aux
			print("c = ", aresta_as_string_only_for_log(x, y), file=sys.stderr)
			peso = gerar_peso(peso_inf, peso_sup, peso_int, pesos)
			incluir_aresta(verts, x, y, peso, pesos)
			vizinho_de_menor[u] = True
			c = c + 1

	log_grafo(n, verts)                                               # Log do grafo final gerado
	print("m: ", m, " p: ", p, " l: ", l, " c: ", c, file=sys.stderr) # Log das quantidades de arestas geradas
	print("total de arestas: ", m + p + l + c, file=sys.stderr)       # Log das quantidades de arestas geradas

	return verts, n, m + p + l + c


def gerar_peso(peso_inf = 0, peso_sup = 0, peso_int = True, pesos = True):
	if pesos:
		if peso_int:
			peso = random.randint(peso_inf, peso_sup)
		else:
			peso = round(random.uniform(peso_inf, peso_sup), 2)
	else:
		peso = 0
	return peso


def incluir_aresta(verts, u, v, peso, pesos):
	verts[u].append((v, peso))          # Incluir aresta (u, v)
	## imprimir_aresta(u, v, peso, pesos)


def imprimir_aresta(u, v, peso, pesos):
	if not pesos:
		print(u + 1, v + 1) # Nos vértices, trabalhando internamente (e no log) no intervalo 0..n - 1, mas imprimindo no intervalo 1..n
	else:
		print(u + 1, v + 1, peso) # Nos vértices, trabalhando internamente (e no log) no intervalo 0..n - 1, mas imprimindo no intervalo 1..n


def imprimir_grafo(verts, n, m, pesos):
	print(n) # Imprimir nr de vértices
	print(m) # Imprimir nr de arestas

	print("#")

	for u in range(0, n):  # Imprimir vértices
		print(u + 1) # Nos vértices, trabalhando internamente (e no log) no intervalo 0..n - 1, mas imprimindo no intervalo 1..n

	print("#")

	for u in range(0, n):  # Imprimir arestas
		for v_peso in verts[u]: ## for v in range(u + 1, n):
			imprimir_aresta(u, v_peso[0], v_peso[1], pesos)


def log_grafo(n, verts):
	for u in range(0, n):
		print(u, ":", verts[u], file=sys.stderr)


def aresta_as_string_only_for_log(u, v):
	return "(",u,",",v,")"


def main():
	argc = len(sys.argv)
	print("argc: ", argc, file=sys.stderr) # print >> sys.stderr, "argc: ", argc

	if argc > 1:
		n = int(sys.argv[1])
	else:
		n = 5

	if argc > 2:
		perc_arest = float(sys.argv[2])
	else:
		perc_arest = 0.5

	if argc > 3:
		orientado = literal_eval(sys.argv[3])
	else:
		orientado = True

	if argc > 4:
		conexo_ou_semi = literal_eval(sys.argv[4])
	else:
		conexo_ou_semi = True

	if argc > 5:
		pesos = literal_eval(sys.argv[5])
	else:
		pesos = False

	if argc > 6:
		peso_inf = float(sys.argv[6])
	else:
		peso_inf = 0

	if argc > 7:
		peso_sup = float(sys.argv[7])
	else:
		peso_sup = 0

	if argc > 8:
		peso_int = literal_eval(sys.argv[8])
	else:
		peso_int = True

	if argc > 9:
		perc_arest_arest_anti_paralel = float(sys.argv[9])
	else:
		perc_arest_arest_anti_paralel = 0.01

	if argc > 10:
		perc_vert_arests_laco = float(sys.argv[10])
	else:
		perc_vert_arests_laco = 0.005

	if argc > 11:
		aciclico_se_orientado = literal_eval(sys.argv[11])
	else:
		aciclico_se_orientado = False

	verts, N, M= gerar_grafo(n, perc_arest, orientado, conexo_ou_semi, pesos, peso_inf, peso_sup, peso_int, perc_arest_arest_anti_paralel, perc_vert_arests_laco, aciclico_se_orientado)

	imprimir_grafo(verts, N, M, pesos)


if __name__ == "__main__":
	main()
